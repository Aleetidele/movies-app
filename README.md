# The challenge

This is a technical test app for this challenge you will need to fix the following issues:
The movies-app it the react front end application
The api folder contains a small express api

1. Several images from the app are not loading properly
2. Fix the TODO list in order for the react application
3. Add the required unit testing for your fixes
4. Each solution needs to have a unique commit
5. Move to the backend challenge and complete the TODO list
6. Add to the front end a new feature to sell movies to another studio
7. Add a log (plain text file) with the issues that you faced during the test and how you solved them
8. (Optional) fix any vulnerabilities you find

## How you will do it?
You need to complete all the items listed before and push the changes to a new branch. That branch must be named ``[NameSurname]``

## Environment file
> Backend

```bash
ACCESS_TOKEN_SECRET=swsh23hjddnns
ACCESS_TOKEN_LIFE=2 hours
```

> Frontend

```bash
REACT_APP_API_URL=http://localhost:3000
REACT_APP_DEFAULT_IMAGE=https://picsum.photos/96/96
```