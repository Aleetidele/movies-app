import { createStyles, makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) =>
  createStyles({
    avatar: {
      width: theme.spacing(8),
      height: theme.spacing(8),
      [theme.breakpoints.up("lg")]: {
        width: theme.spacing(10),
        height: theme.spacing(10),
      },
      [theme.breakpoints.up("xl")]: {
        width: theme.spacing(12),
        height: theme.spacing(12),
      },
    },
    card: {
      borderRadius: 20,
      background: "#20232a",
      boxShadow: "4px 4px 2px #1b1d23, -2px -2px 2px #252931",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "left",
      padding: theme.spacing(2),
      color: "#ffd175",
      [theme.breakpoints.up("md")]: {
        justifyContent: "center",
      },
    },
    title: {
      fontWeight: "600",
      margin: `${theme.spacing(2)}px 0`,
      fontSize: "1.2rem",
      [theme.breakpoints.up("md")]: {
        fontSize: "1.5rem",
      },
    },
    titleYears: {
      fontWeight: "400",
    },
    studioName: {
      fontWeight: "200",
      fontStyle: "italic",
    },
  })
);
