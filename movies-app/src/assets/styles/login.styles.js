import { createStyles, makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) =>
  createStyles({
    card: {
      borderRadius: 20,
      background: "#20232a",
      boxShadow: "4px 4px 2px #1b1d23, -2px -2px 2px #252931",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "left",
      padding: theme.spacing(2),
      [theme.breakpoints.up("md")]: {
        justifyContent: "center",
      },
    },
    logo: {
      filter: "invert(1)",
      opacity: 0.76,
      width: 100,
      marginTop: -10,
    },
    formContainer: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      padding: theme.spacing(2),
    },
    inputContainer: {
      padding: theme.spacing(1),
    },
    rootLabel: {
      color: "#7a7a7a!important",
    },
    inputRoot: {
      color: "#ababab",
    },
    underlineInput: {
      "&:before": {
        borderBottom: "1px solid #5d5d5d",
      },
      "&:after": {
        borderBottom: "2px solid #5d5d5d",
      },
      "&:hover:not(.Mui-disabled):before": {
        borderBottom: "2px solid #404040",
      },
    },
  })
);
