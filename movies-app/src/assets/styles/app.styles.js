import { createStyles, makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) =>
  createStyles({
    app: {
      minHeight: "100vh",
      backgroundColor: "#20232a",
      padding: theme.spacing(4),
    },
  })
);
