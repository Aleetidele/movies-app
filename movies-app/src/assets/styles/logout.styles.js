import { createStyles, makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) =>
  createStyles({
    logoutButton: {
      position: "fixed",
      right: theme.spacing(4),
      bottom: theme.spacing(4),
    },
    icon: {
      height: 30,
      width: 30,
    },
  })
);
