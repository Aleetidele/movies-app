import { createStyles, makeStyles } from "@material-ui/core/styles";
import { yellow } from "@material-ui/core/colors";

export default makeStyles((theme) =>
  createStyles({
    avatar: {
      backgroundColor: yellow[100],
      color: yellow[600],
      "& > img": {
        width: "80%",
        filter: "grayscale(1)",
      },
    },
    button: {
      color: "#1c1d23",
      fontWeight: 600,
      marginTop: theme.spacing(1),
      backgroundColor: "#f9c660",

      "&:hover": {
        backgroundColor: "#d9ae53",
      },
    },
  })
);
