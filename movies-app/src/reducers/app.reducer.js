export const ACTIONS = {
  API_FETCHED: "API_FETCHED",
  API_ERROR: "API_ERROR",
};

export const initialState = ({ studios = [], movies = [] }) => ({
  studios,
  movies,
  error: null,
});

export const reducer = (state, action) => {
  switch (action.type) {
    case ACTIONS.API_FETCHED:
      return {
        ...state,
        ...action.value,
      };
    case ACTIONS.API_ERROR:
      return {
        ...state,
        error: action.value,
      };
    default:
      return state;
  }
};
