import styles from "./assets/styles/app.styles";
import { useAppContext } from "./contexts/app.context";
import Login from "./pages/login";
import Movies from "./pages/movies";
import { MoviesProvider } from "./contexts/movies.context";

export default function App() {
  const { accessToken } = useAppContext();
  const classes = styles();

  return (
    <div className={classes.app}>
      {!accessToken ? (
        <Login />
      ) : (
        <MoviesProvider>
          <Movies />
        </MoviesProvider>
      )}
    </div>
  );
}
