import { render } from "@testing-library/react";
import { AppProvider } from "../../contexts/app.context";
import { MoviesProvider } from "../../contexts/movies.context";

const AllTheProviders = ({ children }) => {
  return (
    <AppProvider>
      <MoviesProvider>{children}</MoviesProvider>
    </AppProvider>
  );
};

const customRender = (ui, options) =>
  render(ui, { wrapper: AllTheProviders, ...options });

// re-export everything
export * from "@testing-library/react";

// override render method
export { customRender as render };
