export const studiosMock = [
  {
    id: 1,
    name: "Disney studios",
    shortName: "Disney",
    logo: "https://cdn.mos.cms.futurecdn.net/qfFFFhnM8LwZnjpTECN3oB.jpg",
    money: 1000,
  },
  {
    id: 2,
    name: "Warner Bros.",
    shortName: "Warner",
    logo: "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/12c6f684-d447-4457-84fa-12033cfd581e/d9z4nxu-626ae303-e830-4b4f-ab8b-4aff7f1bef0f.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzEyYzZmNjg0LWQ0NDctNDQ1Ny04NGZhLTEyMDMzY2ZkNTgxZVwvZDl6NG54dS02MjZhZTMwMy1lODMwLTRiNGYtYWI4Yi00YWZmN2YxYmVmMGYuanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.gtKaGVrDg8gzU7QFThusbHJw2d6bKgnDauezUcZo-1A",
    money: 900,
  },
  {
    id: 3,
    name: "Sony Pictures",
    shortName: "Sony",
    logo: "https://logoeps.com/wp-content/uploads/2013/05/sony-pictures-entertainment-vector-logo.png",
    money: 700,
  },
];

export const moviesMock = [
  {
    id: 11,
    studio_id: 1,
    name: "Nightmare before christmas",
    genre: "HOR",
    img: "https://www.dimanoinmano.it/img/638590/full/libri-per-ragazzi/infanzia/nightmare-before-christmas.jpg",
    price: 600,
    years: 19,
  },
  {
    id: 12,
    studio_id: 2,
    name: "Aladdin",
    genre: "ANI",
    url: "https://www.lainformacion.com/files/article_default_content/uploads/2018/11/23/5bf84292d23b5.jpeg",
    price: 4123,
    years: 5,
  },
  {
    id: 13,
    studio_id: 3,
    name: "The avengers",
    genre: "HER",
    url: "https://static.wikia.nocookie.net/marvelcinematicuniverse/images/2/2b/The_Avengers_Poster.png/revision/latest?cb=20150610135853&path-prefix=es",
    price: 533,
    years: 54,
  },
];
