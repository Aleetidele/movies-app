import { render, waitFor, fireEvent } from "../../utils/test-app.utils";
import Login from "../../../pages/login";

describe("Login Page Component", () => {
  let values;

  beforeEach(() => {
    values = {
      username: "john",
      password: "test123test",
      access_token: Math.random(),
    };
  });

  test("Should try to login successfully", async () => {
    fetch.mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve({ access_token: values.access_token }),
      })
    );

    const { getByTestId, getByText } = render(<Login />);

    fireEvent.change(getByTestId("input-username"), {
      target: { value: values.username },
    });
    fireEvent.change(getByTestId("input-password"), {
      target: { value: values.password },
    });
    fireEvent.click(getByText(/login/i));

    expect(fetch).toHaveBeenCalledTimes(1);
    expect(fetch).toHaveBeenCalledWith(
      `${process.env.REACT_APP_API_URL}/login`,
      expect.objectContaining({
        body: new URLSearchParams({
          username: values.username,
          password: values.password,
        }),
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        method: "POST",
      })
    );

    await waitFor(() =>
      expect(localStorage.getItem("accessToken")).toBe(
        values.access_token.toString()
      )
    );
  });
});
