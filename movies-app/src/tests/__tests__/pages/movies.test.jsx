import { act, fireEvent, render } from "../../utils/test-app.utils";
import * as useMovieApi from "../../../hooks/use-movie-api";
import Movies from "../../../pages/movies";
import { moviesMock, studiosMock } from "../../__mocks__/movies.mock";
import { queryByText, waitFor } from "@testing-library/react";

describe("Movies Page Component", () => {
  afterAll(() => {
    jest.spyOn(useMovieApi, "default").mockReturnValue([[], []]);
  });

  describe("With movies and studios", () => {
    beforeEach(() => {
      jest
        .spyOn(useMovieApi, "default")
        .mockReturnValue({ movies: moviesMock, studios: studiosMock });
    });

    test("Should correctly retrieve movies and studios", () => {
      const { getByText } = render(<Movies />);

      getByText(/nightmare before christmas/i);
      getByText(/disney studios/i);
    });

    test("Should correctly sell a movie to another studio", async () => {
      const { getByText, getAllByText, queryByText } = render(<Movies />);

      getByText("Disney studios");

      await act(async () => {
        fireEvent.click(getAllByText(/sell movie/i)[0]);
        await waitFor(() =>
          fireEvent.click(getByText(/sell to sony pictures/i))
        );
        fireEvent.click(getByText("Agree"));
      });

      expect(queryByText("Disney studios")).toBeNull();
      expect(getAllByText(/sony pictures/i).length).toBe(2);
    });
  });
});
