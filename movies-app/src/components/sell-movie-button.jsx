import { useState } from "react";
import { Button, ListItemAvatar } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import styles from "../assets/styles/sell-movie.styles";
import { useAppContext } from "../contexts/app.context";
import DisneyLogo from "../assets/images/walt-disney-logo.png";
import SonyLogo from "../assets/images/sony-logo.png";
import WarnerLogo from "../assets/images/warner-logo.png";
import { useMoviesContext } from "../contexts/movies.context";

const STUDIOS = {
  1: DisneyLogo,
  2: WarnerLogo,
  3: SonyLogo,
};

const mapIcon = (movie) => ({ ...movie, icon: STUDIOS[movie.id] });

export default function SellMovieButton({ movieId, studioId }) {
  const { accessToken } = useAppContext();
  const { studios, movieSoldToAnotherStudio } = useMoviesContext();
  const [movieModalIsOpened, setMovieModalIsOpened] = useState(false);
  const [selectedStudioId, setSelectedStudioId] = useState(null);
  const classes = styles();

  const studiosList = studios.filter(({ id }) => id !== studioId).map(mapIcon);

  const sellMovie = async () => {
    await (
      await fetch(
        `${
          process.env.REACT_APP_API_URL
        }/transfer/${movieId}?${new URLSearchParams({
          accessToken,
        })}`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
          body: new URLSearchParams({ studioId: selectedStudioId }),
        }
      )
    ).json();

    movieSoldToAnotherStudio(movieId, selectedStudioId);
    setSelectedStudioId(null);
    setMovieModalIsOpened(false);
  };

  return (
    <>
      <Button
        variant="contained"
        className={classes.button}
        onClick={() => setMovieModalIsOpened(true)}
      >
        Sell movie
      </Button>
      <Dialog
        onClose={() => setMovieModalIsOpened(false)}
        aria-labelledby="simple-dialog-title"
        open={movieModalIsOpened}
      >
        <DialogTitle id="simple-dialog-title">
          You are going to sell the movie to another Studio
        </DialogTitle>
        <List>
          {studiosList.map(({ id, name, icon }) => (
            <ListItem button onClick={() => setSelectedStudioId(id)} key={name}>
              <ListItemAvatar>
                <Avatar className={classes.avatar}>
                  <img src={icon} alt={name} />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={`Sell to ${name}`} />
            </ListItem>
          ))}
        </List>
      </Dialog>
      <Dialog
        onClose={() => setSelectedStudioId(null)}
        aria-labelledby="simple-dialog-title"
        open={selectedStudioId !== null}
      >
        <DialogTitle id="simple-dialog-title">
          Are you sure you want to sell this movie to{" "}
          <b>{studiosList.find(({ id }) => id === selectedStudioId)?.name}</b>
        </DialogTitle>
        <DialogActions>
          <Button onClick={() => setSelectedStudioId(null)} color="primary">
            Disagree
          </Button>
          <Button onClick={sellMovie} color="primary" autoFocus>
            Agree
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
