import { Avatar, Card, Grid, Typography } from "@material-ui/core";
import styles from "../assets/styles/movie-card.styles";
import SellMovieButton from "./sell-movie-button";

export default function MovieCard({ movie, studioName }) {
  const classes = styles();

  return (
    <Grid item xs={12} sm={6} lg={4}>
      <Card className={classes.card}>
        <Avatar alt={movie.name} src={movie.img} className={classes.avatar}>
          <img src={process.env.REACT_APP_DEFAULT_IMAGE} alt="" />
        </Avatar>
        <Typography className={classes.title} variant="h5" paragraph={false}>
          {movie.name}{" "}
          {movie.years !== null && (
            <Typography className={classes.titleYears} variant="caption">
              {movie.years} years
            </Typography>
          )}
        </Typography>
        <Typography className={classes.studioName} variant="subtitle2">
          {studioName}
        </Typography>
        <SellMovieButton movieId={movie.id} studioId={movie.studio_id} />
      </Card>
    </Grid>
  );
}
