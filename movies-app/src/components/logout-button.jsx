import Fab from "@material-ui/core/Fab";
import LogoutIcon from "../assets/icons/logout.icon";
import styles from "../assets/styles/logout.styles";
import { useAppContext } from "../contexts/app.context";

export default function LogoutButton() {
  const { logout } = useAppContext();
  const classes = styles();

  return (
    <Fab color="secondary" className={classes.logoutButton} onClick={logout}>
      <LogoutIcon className={classes.icon} />
    </Fab>
  );
}
