import { Grid } from "@material-ui/core";
import { useMoviesContext } from "../contexts/movies.context";
import MovieCard from "./movie-card";

export default function MoviesContainer() {
  const { movies, studios } = useMoviesContext();

  const getStudioName = (movieId) =>
    studios?.find((studio) => studio.id === movieId)?.name;

  return (
    <Grid container justifyContent="center" alignItems="center" spacing={4}>
      {movies?.map((movie) => (
        <MovieCard
          key={movie.id}
          movie={movie}
          studioName={getStudioName(movie.studio_id)}
        />
      ))}
    </Grid>
  );
}
