import { Button } from "@material-ui/core";
import { useAppContext } from "../contexts/app.context";

export default function LoginButton({ username, password }) {
  const { authenticate } = useAppContext();

  const login = async () => {
    if (username.length < 3 || password < 3) {
      return;
    }

    const { access_token } = await (
      await fetch(`${process.env.REACT_APP_API_URL}/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: new URLSearchParams({ username, password }),
      })
    ).json();

    authenticate(access_token);
  };

  return (
    <Button variant="contained" color="default" onClick={login}>
      Login
    </Button>
  );
}
