import { useEffect, useReducer } from "react";
import { ACTIONS, initialState, reducer } from "../reducers/app.reducer";
import { useAppContext } from "../contexts/app.context";

export default function useMovieApi(...urisToRetrieve) {
  const { accessToken, logout } = useAppContext();
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    (async () => {
      try {
        const [studios, movies] = await Promise.all(
          (
            await Promise.all(
              urisToRetrieve.map((uri) =>
                fetch(
                  `${process.env.REACT_APP_API_URL}${uri}?${new URLSearchParams(
                    {
                      accessToken,
                    }
                  )}`
                )
              )
            )
          ).map((response) => response.json())
        );

        dispatch({ type: ACTIONS.API_FETCHED, value: { studios, movies } });
      } catch (error) {
        dispatch({
          type: ACTIONS.API_ERROR,
          value: error,
        });
        logout();
      }
    })();
    // eslint-disable-next-line
  }, []);

  return state;
}
