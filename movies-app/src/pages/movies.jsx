import { useLayoutEffect } from "react";
import useMovieApi from "../hooks/use-movie-api";
import LogoutButton from "../components/logout-button";
import MoviesContainer from "../components/movies-container";
import { useMoviesContext } from "../contexts/movies.context";

export default function Movies() {
  const { setMovies, setStudios } = useMoviesContext();
  const { movies, studios } = useMovieApi("/studios", "/movies");

  useLayoutEffect(() => {
    setMovies(movies);
    setStudios(studios);
    // eslint-disable-next-line
  }, [movies]);

  return (
    <>
      <MoviesContainer />
      <LogoutButton />
    </>
  );
}
