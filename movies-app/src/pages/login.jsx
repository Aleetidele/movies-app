import { TextField } from "@material-ui/core";
import MovieIcon from "../assets/images/movie-icon.png";
import styles from "../assets/styles/login.styles";
import { useState } from "react";
import LoginButton from "../components/login-button";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const classes = styles();

  return (
    <div className={classes.card}>
      <img
        loading="lazy"
        src={MovieIcon}
        alt="Cinema logo"
        className={classes.logo}
      />
      <div className={classes.formContainer}>
        <div className={classes.inputContainer}>
          <TextField
            value={username}
            onChange={({ target: { value } }) => setUsername(value)}
            label="Username"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
                focused: classes.rootLabel,
              },
            }}
            InputProps={{
              classes: {
                root: classes.inputRoot,
                underline: classes.underlineInput,
              },
            }}
            inputProps={{
              "data-testid": "input-username",
            }}
          />
        </div>
        <div className={classes.inputContainer}>
          <TextField
            value={password}
            onChange={({ target: { value } }) => setPassword(value)}
            label="Password"
            type="password"
            InputLabelProps={{
              shrink: true,
              classes: {
                root: classes.rootLabel,
                focused: classes.rootLabel,
              },
            }}
            InputProps={{
              classes: {
                root: classes.inputRoot,
                underline: classes.underlineInput,
              },
            }}
            inputProps={{
              "data-testid": "input-password",
            }}
          />
        </div>
        <div className={classes.inputContainer}>
          <LoginButton username={username} password={password} />
        </div>
      </div>
    </div>
  );
}
