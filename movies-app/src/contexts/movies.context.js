import { createContext, useContext, useState } from "react";

const MoviesContext = createContext({
  movies: null,
  studios: null,
  setMovies: () => {},
  setStudios: () => {},
  movieSoldToAnotherStudio: () => {},
});

const { Provider } = MoviesContext;

export const MoviesProvider = ({ children }) => {
  const [movies, setMovies] = useState([]);
  const [studios, setStudios] = useState([]);

  const movieSoldToAnotherStudio = (movieId, newStudioId) => {
    setMovies(
      movies.map((movie) => ({
        ...movie,
        studio_id: movie.id === movieId ? newStudioId : movie.studio_id,
      }))
    );
  };

  return (
    <Provider
      value={{
        movies,
        studios,
        setMovies,
        setStudios,
        movieSoldToAnotherStudio,
      }}
    >
      {children}
    </Provider>
  );
};

export const useMoviesContext = () => {
  const context = useContext(MoviesContext);

  if (!context) {
    throw new Error("useMoviesContext must be used within a MoviesProvider");
  }

  return context;
};
