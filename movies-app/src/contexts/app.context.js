import { createContext, useContext, useEffect, useState } from "react";

const AppContext = createContext({
  accessToken: null,
  isUserLogged: () => false,
  authenticate: () => {},
  logout: () => {},
});

const { Provider } = AppContext;

export const AppProvider = ({ children }) => {
  const [accessToken, setAccessToken] = useState(null);

  const authenticate = (accessToken) => {
    setAccessToken(accessToken);
    localStorage.setItem("accessToken", accessToken);
  };

  const logout = () => {
    setAccessToken(null);
    localStorage.removeItem("accessToken");
  };

  const isUserLogged = () => localStorage.getItem("accessToken") !== null;

  useEffect(() => {
    const savedAccessToken = localStorage.getItem("accessToken");

    if (savedAccessToken !== null) {
      setAccessToken(savedAccessToken);
    }
  }, []);

  return (
    <Provider value={{ accessToken, isUserLogged, authenticate, logout }}>
      {children}
    </Provider>
  );
};

export const useAppContext = () => {
  const context = useContext(AppContext);

  if (!context) {
    throw new Error("useAppContext must be used within a AppProvider");
  }

  return context;
};
