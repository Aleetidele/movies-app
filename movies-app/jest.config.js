const baseConfig = {
  moduleDirectories: ["node_modules", "src"],
  testEnvironment: "jsdom",
  transform: {
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/src/tests/utils/file-transformer.utils.js",
    "^.+\\.(js|jsx)$": ["babel-jest"],
  },
};

module.exports = {
  ...baseConfig,
  projects: [
    {
      displayName: "app",
      testMatch: ["<rootDir>/src/tests/__tests__/**/*.test.jsx"],
      setupFilesAfterEnv: ["<rootDir>/jest.env.js"],
      ...baseConfig,
    },
  ],
};
