require("dotenv").config();
const { localStorage } = require("./src/tests/__mocks__/local-storage.mock");

global.localStorage = localStorage;

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: jest.fn(),
  })
);

beforeEach(() => {
  global.localStorage.clear();

  window.location = {
    assign: jest.fn(),
    href: null,
  };
});
