import { studios, movies } from "../../constants/studio.constants.mjs";

export default class MovieService {
  static movies = movies;

  static getStudios() {
    return studios;
  }

  static getMovies() {
    return MovieService.movies;
  }

  static getMoviesAge() {
    return MovieService.movies.map(({ id, years }) => ({ id, years }));
  }

  static setMovieTransfer(movieId, studioId) {
    if (!studios.map(({ id }) => id).includes(studioId)) {
      throw "The provided studioId is not valid";
    }

    const movieIndex = MovieService.movies.findIndex(
      ({ id }) => id === movieId
    );

    if (movieIndex === -1) {
      throw "The provided movieId is not valid";
    }

    MovieService.movies[movieIndex] = {
      ...MovieService.movies[movieIndex],
      studio_id: studioId,
    };
  }
}
