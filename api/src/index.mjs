import dotenv from "dotenv";
import express from "express";
import cors from "cors";
import { login, verify } from "./authentication/index.mjs";
import MovieService from "./services/movie.service.mjs";

dotenv.config();
const app = express();

app.use(cors());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.get("/studios", verify, (req, res) => {
  try {
    res.json(MovieService.getStudios());
  } catch (error) {
    res.status(500).send(error);
  }
});

app.get("/movies", verify, (req, res) => {
  try {
    res.json(MovieService.getMovies());
  } catch (error) {
    res.status(500).send(error);
  }
});

app.get("/movies-age", verify, (req, res) => {
  try {
    res.json(MovieService.getMoviesAge());
  } catch (error) {
    res.status(500).send(error);
  }
});

// \\d+ prevent from url injection hacking
app.post("/transfer/:movieId(\\d+)", verify, (req, res) => {
  try {
    const { movieId } = req.params;
    const { studioId } = req.body;

    MovieService.setMovieTransfer(parseInt(movieId), parseInt(studioId));

    res.json({ status: "OK" });
  } catch (error) {
    res.status(500).send(error);
  }
});

app.post("/login", login);

app.listen(3000);
