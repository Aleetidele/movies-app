import jwt from "jsonwebtoken";
import { users } from "../../constants/users.constants.mjs";

export const login = (req, res) => {
  const { username, password } = req.body;

  const user = users.find(
    (user) => user.username === username && user.password === password
  );

  if (!user) {
    return res.status(401).json({ error: "Invalid user credentials" });
  }

  res.json({
    access_token: jwt.sign({ username }, process.env.ACCESS_TOKEN_SECRET, {
      expiresIn: process.env.ACCESS_TOKEN_LIFE,
    }),
  });
};

export const verify = (req, res, next) => {
  const { accessToken } = req.query;

  if (!accessToken) {
    return res
      .status(403)
      .json({ error: "Access token has not been provided" });
  }

  try {
    jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET);
    next();
  } catch (e) {
    return res.status(401).json({ error: "Access token is not valid" });
  }
};
